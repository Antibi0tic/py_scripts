### Description

This repository contains some python scripts for internal usage. 
### Scripts
* [image_composer_A4_4.py] - scans specified directory for images (JPG, PNG files) and generate new imagies as composition of 4 imagies on A4 canvas (for easier printing).
* [image_composer_A4_8.py] - same as above, but takes 8 imagies in one composition.

### Usage

```sh
$ python image_composer_A4_4.py "path_to_directory_with_images"
```

License
----
MIT

**Free Software, Hell Yeah!**

[//]: #


   [image_composer_A4_4.py]: <https://github.com/>
   [image_composer_A4_8.py]: <https://github.com/>