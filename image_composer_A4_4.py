import sys
import os.path
from datetime import datetime
from PIL import Image, ImageDraw

def compose_images(imgs, result_path):
    max_w = 0;
    max_h = 0;
    min_aspect_ratio = 14.8 / 10.5
    for i in imgs:
        im = Image.open(i)
        w = max(im.size)
        im.close()
        if w > 3000:
            w = 3000       
        max_w = max(w, max_w)
    max_h = round(max_w / min_aspect_ratio)    
    ri = Image.new("RGB", (max_w * 2 + 2, max_h * 2 + 2), (255, 255, 255))
    x = 0
    y = 0
    for i in imgs:
        im = Image.open(i)
        r = float(im.size[0]) / float(im.size[1])
        if r > min_aspect_ratio:            
            target_w = max_w
            target_h = round(max_w / r)
        else:
            target_w = round(max_h * r)
            target_h = max_h
            
        tmp = im.resize((target_w, target_h))
        offset_x = round((max_w - target_w)/2)
        offset_y = round((max_h - target_h)/2)
        ri.paste(tmp, (x + offset_x, y + offset_y))
        
        y += max_h + 2;
        if y > max_h + 2:
            x = max_w + 2
            y = 0
    draw = ImageDraw.Draw(ri)
    draw.rectangle([(max_w, 0), (max_w + 2, max_h * 4 + 6)], fill = (0,0,0), outline = (0,0,0))
    draw.rectangle([(0, max_h), (max_w * 2 + 2, max_h + 2)], fill = (0,0,0), outline = (0,0,0))
    del draw
    result_rotated = ri.rotate(90, 0, 1)
    result_rotated.save(result_path)
    return result_path

if __name__ == "__main__":
    img_path = '.'
    if len(sys.argv) > 1:
        img_path = sys.argv[1]

    images = []
    if os.path.exists(img_path) and os.path.isdir(img_path):
        print('Searching for imagies in folder:\n' + img_path)
    else:
        print('Folder not exests:\n' + img_path)
        sys.exit(1)
        
    images = [x.path for x in os.scandir(img_path)
              if x.is_file() and not x.name.startswith('_') and
              x.name.lower().endswith(('.jpg', '.png'))]

    print('\nFound imagies: ', len(images))
    for i in images:
        print(i)
    
    composed_images = []
    time_prefix = datetime.now().strftime("%Y%m%d_%H%M%S") 
    result_path = os.path.join(img_path, '_composed_A4_4_' + time_prefix)
    if not(os.path.exists(result_path) and os.path.isdir(result_path)):
        os.mkdir(result_path)
   
    while len(images) >= 4:
        imgs = images[:4]
        del images[:4]
        result_name = os.path.join(result_path, time_prefix + str(len(composed_images) + 1) + '_composed.jpg')
        composed_images.append(compose_images(imgs, result_name))

    print('\nComposed images: ')
    for i in composed_images:
        print(i)
